import AppBarEdit from './AppBarEdit'
import FeatureEditor from './FeatureEditor'

export const Appbar = AppBarEdit
export const Detail = FeatureEditor
