import React from 'react'
import { connect } from 'react-redux'

import { default as MuiAppBar } from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import ShareIcon from 'material-ui-icons/Share'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import MenuIcon from 'material-ui-icons/Menu'
import SearchIcon from 'material-ui-icons/Search'
import CloseIcon from 'material-ui-icons/Close'

import { openBrowseMode, openSearch, openShare, setDrawerOpen } from '../../store'

const AppBarFeature = ({
  feature,
  openBrowseMode,
  openSearch,
  openShare,
  setDrawerOpen,
}) => {
  return <div className='appbar custom-appbar'>
    <IconButton onClick={setDrawerOpen}>
      <MenuIcon />
    </IconButton>

    <div className='appbar-stretch'>{feature.name || 'Some Feature'}</div>

    <IconButton onClick={() => openSearch(feature.name)}>
      <SearchIcon />
    </IconButton>

    <IconButton onClick={() => openBrowseMode()}>
      <CloseIcon />
    </IconButton>
  </div>
}

const mapStateToProps = ({ features, control }) => {
  return {
    feature: features[control.featureId],
  }
}

const mapDispatchToProps = {
  openBrowseMode,
  openSearch,
  openShare,
  setDrawerOpen,
}

export default connect(mapStateToProps, mapDispatchToProps)(AppBarFeature)
