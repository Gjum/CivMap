import AppBarFeature from './AppBarFeature'
import FeatureInfo from './FeatureInfo'

export const Appbar = AppBarFeature
export const Detail = FeatureInfo
