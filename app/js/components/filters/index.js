import AppBarFilters from './AppBarFilters'
import FiltersControl from './FiltersControl'

export const Appbar = AppBarFilters
export const Detail = FiltersControl
